//github : https://github.com/qingfeng346/Scorpio-CSharp
namespace Scorpio {
    public static class Version {
        public const string version = "2.0.8";
        public const string date = "2020-06-10";
    }
}
